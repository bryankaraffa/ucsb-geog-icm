# University of California, Santa Barbara Interactive Campus Map (UCSB ICM)
#### Developed by the [UCSB Department of Geography](http://geog.ucsb.edu)

This is the master repository for the UCSB Interactive Campus Map (often referred to as `ucsb-geog-icm` or UCSB ICM).

## Live URL: [http://map.geog.ucsb.edu/](http://map.geog.ucsb.edu/)

## Getting Started

#### What is the UCSB Geography Interactive Campus Map (UCSB ICM) ?
The UCSB Interactive Campus Map (ICM) has been an ongoing project supported by the UCSB Geography Department aimed at developing a beneficial map resource for the University of California, Santa Barbara.  The UCSB ICM has been entirely developed by students at UCSB and gone through many improvements since it's initial release in 2010. We are continually developing the map, adding features and making it a better resource for UCSB. We welcome all who are interested in contributing and would love to see you at the next meeting!

#### I heard something about internships and/or work experience through the UCSB Interactive Campus Map project?

In short, there are always things that need to be done to maintain and improve the UCSB ICM.  We have a varying array of tasks that we invite students to help us complete and contribute to the overall development of the UCSB ICM.  A full list of documented issues/tasks available online -- https://bitbucket.org/bryankaraffa/ucsb-geog-icm/issues?status=new&status=open .

On top of the already documented tasks, we can provide tools and resources to those interested in publishing their own maps and layer ideas online and making them interactive.  Due to the basic GIS knowledge required for most tasks, we are more able to cater to students that have taken the UCSB GEOG176 series or are familiar with GIS.  However, we do have non-GIS related tasks posted that are important as well.

Feel free to drop by our office contact us if you have any questions about the UCSB ICM!

# Please visit our [Wiki here](https://bitbucket.org/bryankaraffa/ucsb-geog-icm/wiki) for complete documentation!